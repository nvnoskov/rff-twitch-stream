# Home Project from StreamLabs

Link to demo [http://rff-twitch-stream.herokuapp.com/](http://rff-twitch-stream.herokuapp.com/)
Video [https://youtu.be/jwl79d1slhY](https://youtu.be/jwl79d1slhY)
## Intro

For starters, it was an interesting objective for me.
I didn't work with Twitch API before so I had to dive in it and the first challenge here was that they had several versions of API and most examples in the open-source were based on Twitch API v5 (kraken). Therefore I started to develop the program with this version.
This API has method to search streamers. That feature is more useful to users because they can find the streamer by name when they don't know the exact name.
However, in the new API (helix) we don't have similar API instead we can make some functionality for browse with the list of games and streams so users can choose what they would like to watch. But I didn't and regret about it everytime when I tested my app. :)

### Design choices
First of all for the platform I choose NodeJS because for me it's the easiest way of prototyping something and see how it works on stage.

I've found an official example of Twitch Authentication on [GitHub/twitchdev](https://github.com/twitchdev/authentication-samples/tree/master/node) so I use it as a template.
It wasn't hard to implement the two pages with Login and Streamer page where I had to embed the chat and the live stream.
However, the list of 10 the most recent events of the streamer was really unclear to me, especially when you mentioned WebSocket. This led me away.
When I read the documentation of WebSockets at TwitchAPI I couldn't understand, why the only possibility to get all the streamer`s events is to be a streamer or a streamer should authorize in your app with a full scope of grants.
So I choose another way.

Public information about streamer can be obtained by Webhooks.
It is necessary to register webhook on Twitch API and receive messages, that's all. I implemented the WebSocket server (socket.io) on the backend.
I had trouble with receiving webhooks from twitch. Firstly I don't find the Webhook Flow where webhook confirmation is described

For communication between server and client, I used the chat rooms.
When a user opens the streamer page he is being subscribed to 3 types of events: follows stream changes, user changes. He can manage subscription on the page.
There is only one connection between backend and Twitch. When the second user opens same streamer he just join to socker.io rooms and will be received events for this streamer.




### Unresolved Problems (I did not have enough time)

- Remember user input and last search
- Twitch login. Since I use the V5(kraken) I need accessToken from auth to use it in the API calls. But in Helix, I don't use it at all, just client ID of my application. Except for greetings, of course.
- Refactoring.
- Tests
- Unsubscribe webhooks


# Additional answers

## How would you deploy the above on AWS? (ideally, a rough architecture diagram will help)

I didn’t work much with AWS before, only with DigitalOcean, Google Platform Engine,
so I'm not sure in my understanding of AWS Services, but I`ll try.
Main components:
Static  (S3)
Backend (Serverless)
API Gateway
If it takes this functionality we can use, just serverless on AWS
because it's the easiest and the cheapest way to organize backend functions.

![AWS](/public/img/streamlabs.v1.png)



## Where do you see bottlenecks in your proposed architecture and how would you approach scaling this app starting from 100 reqs/day to 900MM reqs/day over 6 months?

As we have two parts of this functional, we should think about performance problem separately.

Frontend
The frontend which should be PWA (lightweight, use cache and web workers).

Backend
For production, I prefer to use strictly typed language as Go or Rust. For performance and decrease memory consumption.
We need to make the back-end horizontally scaling and SOA, and should separate backend into independent parts.
Webhook functionality is just for processing request of the Twitch API for all streamers.
Another part will work with our frontend (WebSocket server).

And in case we have 900MM reqs/day we should have some buffer between these two parts.
It may be some message queue (SQS, Kafka, RabbitMQ, etc.) because it is very easy to scale it and give us the flexibility to manipulate with data.
We can have as many front-ends as we want  which will be simply  subscribed to the channels and receive messages and does few API’s calls

And we should keep in mind the subscription limits:

`Limits: Each client ID can have at most 10,000 subscriptions. Also, you can subscribe to the same topic at most 3 times. To increase either of these limits, please fill out the form at https://dev.twitch.tv/limit-increase.`

I tried to draw a complex scheme with SQS, but the lack of experience with AWS I don't know how it communicates with clients. Strayforward or through API Gateway.

But I'll dive into AWS Services for sure.
