/*
Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

    http://aws.amazon.com/apache2.0/

or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

*/

// Define our dependencies
const express        = require('express');
const session        = require('express-session');
const passport       = require('passport');
const OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
const request        = require('request');
const cors = require('cors');
const path = require('path');

const http = require('http');
const socketio = require('socket.io');
const axios = require('axios')
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 5000

// Define our constants, you will change these with your own
const TWITCH_CLIENT_ID = 'mre0zt2y1r6vntlrdmi0r3lr6r3wtz';
const TWITCH_SECRET = '5vy2cxzhxkr52d1v16m0wzl6y527zj';
const SESSION_SECRET   = 'nsdfjkT^&%*H@ND(#G';
const CALLBACK_URL = 'https://rff-twitch-stream.herokuapp.com/auth/twitch/callback';  // You can run locally with - http://localhost:3000/auth/twitch/callback

// Initialize Express and middlewares
var app = express();
app.use(session({secret: SESSION_SECRET, resave: false, saveUninitialized: false}));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
app.use(passport.initialize());
app.use(passport.session());

// Override passport profile function to get user profile from Twitch API
OAuth2Strategy.prototype.userProfile = function(accessToken, done) {
  var options = {
    url: 'https://api.twitch.tv/helix/users',
    method: 'GET',
    headers: {
      'Client-ID': TWITCH_CLIENT_ID,
      'Accept': 'application/vnd.twitchtv.v5+json',
      'Authorization': 'Bearer ' + accessToken
    }
  };

  request(options, function (error, response, body) {
    if (response && response.statusCode == 200) {
      done(null, JSON.parse(body));
    } else {
      done(JSON.parse(body));
    }
  });
}

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

passport.use('twitch', new OAuth2Strategy({
    authorizationURL: 'https://id.twitch.tv/oauth2/authorize',
    tokenURL: 'https://id.twitch.tv/oauth2/token',
    clientID: TWITCH_CLIENT_ID,
    clientSecret: TWITCH_SECRET,
    callbackURL: CALLBACK_URL,
    scope: 'user_read',
    state: true
  },
  function(accessToken, refreshToken, profile, done) {
    profile.accessToken = accessToken;
    profile.refreshToken = refreshToken;

    // Securely store user profile in your DB
    //User.findOrCreate(..., function(err, user) {
    //  done(err, user);
    //});

    done(null, profile);
  }
));

// Set route to start OAuth link, this is where you define scopes to request
app.get('/auth/twitch', passport.authenticate('twitch', { scope: 'user_read' }));

// Set route for OAuth redirect
app.get('/auth/twitch/callback', passport.authenticate('twitch', { successRedirect: '/', failureRedirect: '/' }));


app.get('/', (req, res) => {
  if (req.session && req.session.passport && req.session.passport.user) {
    console.log(req.session.passport.user);
    res.render('pages/index', {
      user: req.session.passport.user,
      stream:null
    });
  }else{
    res.redirect('/login');

  }
})

app.get('/login',(req,res)=>{
  res.render('pages/login', {
    user: null,
    stream:null
  });
})

app.get('/webhook/:topic/:room', (req, res) => {
  console.log('Confirm webhook', req.query);
  res.end(req.query['hub.challenge']);
})
app.post('/webhook/:topic/:room', (req, res) => {
  let topic = req.params.topic;
  let message = '';
  switch (topic) {
    case 'follows':
      if (req.body.data.length>0){
        message = `User ${req.body.data[0].from_name} follows ${req.body.data[0].to_name}`;
      }
      break;
    case 'users':
      if (req.body.data.length > 0) {
        message = `User ${req.body.data[0].display_name} follows ${req.body.data[0].description}. View count: ${req.body.data[0].view_count}`;
      }
      break;
      case 'streams':
      if (req.body.data.length > 0) {
        message = `User ${req.body.data[0].user_name} changes stream title to ${req.body.data[0].title}`;
      }
      break;
  }
  console.log("SOCKET: ", `Send message ${message} to room ${req.params.room + '.' + topic}`)
  io.to(req.params.room+'.'+topic).emit('event', { message: message, topic: req.params.topic, post: req.body });
  res.end();
})
app.delete('/favorite/:stream', (req, res) => {

})
app.get('/watch/:stream/:user_id',(req,res)=>{
  if (req.session && req.session.passport && req.session.passport.user) {
    res.render('pages/watch', {
      user: req.session.passport.user,
      stream: {
        name: req.params.stream,
        user_id: req.params.user_id
      }
    });
  } else {
    res.redirect('/login');
  }
})

const server = http.Server(app);
const io = socketio(server);

// will store subscribed channels with count of subscribers
var subscribedChannels = {}

io.on('connection', function (socket) {
  socket.emit('event', { message: 'Connected to the EventService' });
  socket.on('subscribe', function (data) {
    console.log(data);
    if (subscribedChannels[data.user_id]===undefined){
      subscribeToChannel(data.user_id, 'follows');
      socket.join(data.user_id +'.follows');
      subscribeToChannel(data.user_id, 'streams');
      socket.join(data.user_id + '.streams');
      subscribeToChannel(data.user_id, 'users');
      socket.join(data.user_id + '.users');
    }else{
      subscribedChannels[data.user_id]+=1;
    }

  });
  socket.on('join', function (data) {
    socket.join(data.room);
    console.log("SOCKET: ", `User ${socket.id} joined room ${data.room}`)
  })
  socket.on('leave', function (data) {
    socket.leave(data.room);
    console.log("SOCKET: ", `User ${socket.id} leaved room ${data.room}`)
  })
});

server.listen(PORT, function () {
  console.log(`Twitch auth sample listening on port ${PORT}!`)
});


function subscribeToChannel(channel,topic) {
  let topicUrl = getUrlForTopic(channel,topic);
  console.log(channel,topic,topicUrl);
  axios.post('https://api.twitch.tv/helix/webhooks/hub',  {
    "hub.mode": "subscribe",
    "hub.topic": topicUrl.topic,
    "hub.callback": topicUrl.callbackUrl,
    "hub.lease_seconds": "86400",
    "hub.secret": "s3cRe7"
  },{
    headers:{
      'Client-ID':TWITCH_CLIENT_ID
    }
  })
    .then((res) => {
      console.log(`statusCode: ${res.status} ${res.statusText}`)

    })
    .catch((error) => {
      console.error(error)
    })
}
function unsubscribeToChannel(channel, topic) {
  let topicUrl = getUrlForTopic(channel, topic);
  axios.post('https://api.twitch.tv/helix/webhooks/hub', {
    "hub.mode": "unsubscribe",
    "hub.topic": topicUrl.topic,
    "hub.callback": topicUrl.callbackUrl,
    "hub.secret": "s3cRe7"
  }, {
    headers: {
      'Client-ID': TWITCH_CLIENT_ID
    }
  })
  .then((res) => {
    console.log(`statusCode: ${res.status} ${res.statusText}`)
    // console.log()
  })
  .catch((error) => {
    console.error(error)
  })
}

function getUrlForTopic(user_id, topic) {
  switch (topic) {
    case 'follows':
      return {
        topic: 'https://api.twitch.tv/helix/users/follows?first=1&to_id=' + user_id,
        callbackUrl: 'https://rff-twitch-stream.herokuapp.com/webhook/follows/' + user_id
      }
    case 'streams':
      return {
        topic: 'https://api.twitch.tv/helix/streams?user_id=' + user_id,
        callbackUrl: 'https://rff-twitch-stream.herokuapp.com/webhook/streams/' + user_id
      }

    case 'users':
      return {
        topic:"https://api.twitch.tv/helix/users?id=" + user_id,
        callbackUrl: 'https://rff-twitch-stream.herokuapp.com/webhook/users/' + user_id
      }
  }
  return {};
}
