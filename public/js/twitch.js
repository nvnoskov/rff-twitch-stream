var clientId = 'mre0zt2y1r6vntlrdmi0r3lr6r3wtz';
var redirectURI = 'http://localhost:3000/auth/twitch/callback';
var scope = 'user_read+chat_login+bits:read+channel_subscriptions+whispers:read';
var ws;


function parseFragment(hash) {
    var hashMatch = function (expr) {
        var match = hash.match(expr);
        return match ? match[1] : null;
    };
    var state = hashMatch(/state=(\w+)/);
    if (sessionStorage.twitchOAuthState == state)
        sessionStorage.twitchOAuthToken = hashMatch(/access_token=(\w+)/);
    return
};

function authUrl() {
    sessionStorage.twitchOAuthState = nonce(15);
    var url = 'https://api.twitch.tv/helix/oauth2/authorize' +
        '?response_type=token' +
        '&client_id=' + clientId +
        '&redirect_uri=' + redirectURI +
        '&state=' + sessionStorage.twitchOAuthState +
        '&scope=' + scope;
    return url
}

// Source: https://www.thepolyglotdeveloper.com/2015/03/create-a-random-nonce-string-using-javascript/
function nonce(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

function searchUsers(searchString){
    $.ajax({
        url: "https://api.twitch.tv/helix/streams?user_login="+searchString,
        type: "GET",
        headers: {
            // "Accept": "application / vnd.twitchtv.v5 + json",
            "Client-ID": clientId,
            // "Authorization": "OAuth " + sessionStorage.twitchOAuthToken
        },
        success: function (response) {
            var data = response.data
            console.log(data);
            if (data.length>0) {
                $('#channels').empty();
                data.forEach(function (channel){
                $('#channels').append(`
    <div class="pure-u-1-2">
      <section class="channel">
       <div class="channel-card">
        <header class="channel-header">
          <img alt="${channel.user_name}" class="channel-avatar" src="${channel.logo}" width="48" height="48">

          <h2 class="channel-title">${channel.title}</h2>

          <p class="channel-meta">
            By &#128279; <a target="_blank" href="${channel.user_name.toLowerCase()}/${channel.user_id}" class="channel-author">${channel.user_name}</a>
              <a class="channel-category channel-category-design" href="#">Followers: ${channel.followers && channel.followers.toLocaleString() || 0}</a>
              <a class="channel-category channel-category-pure" href="#">Viewers: ${channel.viewer_count.toLocaleString() || 0}</a>
          </p>
        </header>
        <div class="channel-images pure-g">
            <div class="pure-u-1 pure-u-md-1-2">
                <button class="button-twitch pure-button pure-u-1" ><i class="fa fa-cog"></i> Set as Favorite</button>
            </div>
            <div class="pure-u-1 pure-u-md-1-2">
                <a href="/watch/${channel.user_name.toLowerCase()}/${channel.user_id}" class="pure-button pure-u-1">Watch ${channel.user_name}</a>
            </div>
          </div>
        <div class="channel-description"><br>
            <a  target="_blank" href="https://www.twitch.tv/${channel.user_name.toLowerCase()}">
                <img alt="" class="pure-img-responsive" src="${channel.thumbnail_url.replace('{width}','1280').replace('{height}',720)}">
            </a>

        </div>
        </div>
      </section></div>
`)
                });
            }else{
                $('#channels').html('<p>Nothing is found</p>');
            }
        }
    })
}

$(function () {

    if (sessionStorage.twitchOAuthToken) {

    //     $.ajax({
    //         url: "https://api.twitch.tv/helix/user",
    //         type: "GET",
    //         headers: {
    //             "Client-ID": clientId,
    //             "Authorization": "OAuth " + sessionStorage.twitchOAuthToken
    //         },
    //         success: function (user) {
    //             $('#topic-label').text("Enter a topic to listen to. For example, to listen to whispers enter topic 'whispers." + user._id + "'");
    //         }
    //     })
    } else {
        // var url = authUrl()
        // $('#auth-link').attr("href", url);
        // $('.auth').show()
        document.location.href='/login'
    }
    var timer;
    $('#search').keyup(function () {
        var value = $(this).val();
        clearTimeout(timer);
        timer = setTimeout(function(){
            searchUsers(value);
        },350)
        // listen($('#topic-text').val());
        // event.preventDefault();
    });

    $('#subbtn').click(function (event) {
        listen($('#topic-text').val());
        event.preventDefault();
    });
    $('#search-btn').click(function (event) {
        searchUsers($('#search').val());
        event.preventDefault();
    });
    $('#chat_embed').height($(document).height());

});
